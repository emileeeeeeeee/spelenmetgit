<?php
    include('ThrowableObject.php');
    include('Dobbelsteen.php');
    include('Munt.php');
    include('Observer.php');
    $munt = new Munt();
    $dobbelsteen = new Dobbelsteen();
    echo 'resultaat: '.$dobbelsteen->throwNow();
    echo '<br>';
    echo 'resultaat: '.$munt->throwNow();
    echo '<br>';
    $observer = new Observer();
    $observer->setObject($munt);
    $observer->observe(10);
    print_r($observer->getResults());
?>