<?php
    abstract class ThrowableObject{
        
        protected $outcomes = array();
        
        public function getOutcomes(){
            return $this->outcomes;
        }
        public function setOutcomes($outcomes){
            $this->outcomes = $outcomes;
        }
        
        public function throwNow(){
            $numberOfPossibilities = count($this->outcomes);
            $result = rand(0,$numberOfPossibilities-1);
            return $this->outcomes[$result];
        }
    }
?>