<?php
    class Observer{
        /** @var ThrowableObject */
        private $object = null;
        private $results = array();
        
        public function getObject(){
            return $this->object;
        }
        public function setObject($object){
            $this->object = $object;
        }
        public function observe($times = 1){
            for ($i = 0;$i<$times; $i++){
                $this->results[$this->object->throwNow()]++;
            }
        }
        public function getResults(){
            return $this->results;
        }
    }
?>